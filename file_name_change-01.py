#!/usr/bin/python3

# import argparse
# https://datatofish.com/rename-file-python/

import os, fnmatch

listOfFiles = os.listdir('./change')
pattern = "2359992*"
for entry in listOfFiles:
    if fnmatch.fnmatch(entry, pattern):
        old_entry = './change/'+entry
        new_entry = './change/'+entry[8:]
        print(f'old: {old_entry}\nnew: {new_entry}\n')
        os.rename(old_entry, new_entry)


# entry = '2359992-file.txt'
# old_entry = entry
# new_entry = entry[8:]
# print(old_entry, new_entry)
# os.rename(old_entry, new_entry)

# listOfFiles = os.listdir('.')
# pattern = "2359992*"
# for entry in listOfFiles:
#     if fnmatch.fnmatch(entry, pattern):
#             print (type(entry),'-',entry)